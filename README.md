# Apresentação Kanban

Projeto criado para apresentação da disciplina Projeto de Carreira, do curso de Ciência da Computação na UFRJ. Esse projeto tem o objetivo de exemplificar o uso do Kanban dentro do GitLab.

## Informações da disciplina

- disciplina: Projeto de Carreira
- Período Letivo: 2023.1
- Professora: Eldanae

## Membros do grupo
- Douglas Lima
- Ilan Araujo
- Jonathan barbêdo
- Pedro Teixeira
- Stefanne dos Santos
